db.rooms.insertOne(
    {
        name: "Single",
        accomodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities",
        rooms_available: 10,
        isAvailable: false
    }
 );

db.rooms.insertMany([
    {
        name: "double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for small family going on vacation",
        rooms_available: 5,
        isAvailable: false
    }
    ]);

db.rooms.insertMany([
    {
        name: "Queen",
        accomodates: 4,
        price: 2000,
        description: "A room with queen sized bed perfect for simple gateway",
        rooms_available: 15,
        isAvailable: false
    }
    ]);

db.rooms.find(
    {
        name: "double"
    }
);

db.rooms.updateOne(
    {
        name: "Queen"
    },
    {
        $set: {rooms_available: 0}
    }
);

db.rooms.deleteMany(
    {
        rooms_available: 0
    },
    {
        name: "Queen",
        accomodates: 4,
        price: 2000,
        description: "A room with queen sized bed perfect for simple gateway",
        rooms_available: 15,
        isAvailable: false
    }
);
